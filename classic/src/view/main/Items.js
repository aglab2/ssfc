Ext.define('TestApp.view.main.Item', {
	extend: 'Ext.form.Panel',
	xtype: 'item',		    
    config: {   
         longtext: '',
         shorttext: '',
         calculus: '',
         defVal: '',
         tlistener: [],
         llistener: [],
         nminValue: 0
    },
    
	frame: false,
    border: false,
    bodyStyle: 'background:transparent',
    
    listeners: {
		afterrender: function () {
			console.log('ar');
			MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        } 
    },
    
    initComponent: function() {
		this.items = [{
			xtype: 'fieldcontainer',
			layout: {
				type: 'hbox',
				align: 'center'
			},
			frame: false,
			border: false,
			bodyStyle: 'background:transparent',
			itemId: "itembox",

			items: [{
				html: this.longtext,
				xtype: 'panel',
				flex: 80,
				bodyStyle: {
					'background': '#ffffff', 
					'padding':'0.6em 0em 0.6em 1em',
					'border-radius':'5px 0px 0px 5px'
				}
			},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 3},{
				html: "<center>\\("+this.shorttext+"\\)</center>",
				xtype: 'panel',
				flex: 10,
				bodyStyle: {
					'background': '#ffffff',
					'padding':'0.6em 0em 0.6em 0em'
				}
			},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 3},{
				xtype: 'numberfield',
				itemId: 'numberfield',
				flex: 40,
				text: this.deftext,
				bodyStyle: {'background': '#ffffff'},
				hideTrigger: true,
				keyNavEnabled: false,
				mouseWheelEnabled: false,
				minValue: this.nminValue,
				value: this.defVal,
				listeners: this.tlistener,
				bodyStyle: {
					'max-width': '0.5em'
				}
			},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 3},{
				html: "<center>\\("+this.calculus+"\\)</center>",
				itemId: "calculus",
				xtype: 'panel',
				flex: 10,
				bodyStyle: {
					'background': '#ffffff',
					'padding':'0.4em 0em 0.4em 0em',
					'border-radius':'0px 5px 5px 0px'
				},
				listeners: this.llistener
			}]
		}],		
		
		this.callParent();
	},
	
	getVal: function (){
		return this.getComponent('itembox').getComponent('numberfield').value;
	},
	setVal: function (val){
		this.getComponent('itembox').getComponent('numberfield').setValue(val);
	},
	set: function(field, val){
		this.getComponent('itembox').getComponent(field).update("<center>\\("+val+"\\)</center>");
		MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	}
	
});
