Ext.define('TestApp.view.main.ItemHolderV', {
	extend: 'Ext.form.Panel',
	xtype: 'itemholderV',
    position: 'fixed',
	
			frame: false,
                        border: false,
                        bodyStyle: 'background:transparent',
    
    requires: [
        'TestApp.view.main.Item',
        'TestApp.view.main.SWindow'
    ],
   
    initComponent: function(){
		this.callParent();
		
		try{
			var main = Ext.ComponentQuery.query('appmain')[0];
			main.setBackground('images/backgroundVM.png');
		}catch(err) { };
	},
	        
    items: [{
        layout: {type: 'vbox', align: 'stretch'},
		frame: false,
		border: false,
        bodyStyle: 'background:transparent',
        itemId: 'itemholderbox',

		defaults: {
			tlistener: {change: function(){
				var ih = this.ownerCt.ownerCt.ownerCt.ownerCt;
				ih.calcQ(this);
				this.focus(undefined, 20);
			}}
		},
		
        items: [{xtype: 'panel', bodyStyle: 'background: transparent', flex: 1000},{
			xtype: 'item',
			width: "100%",
			flex: 1,
			resizable: true,
			longtext: "Reservoir thickness",
			shorttext: "h",
			calculus: "ft",
			itemId: "h",
			defVal: 1
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Reservoir permeability",
			shorttext: "k",
			calculus: "mD",
			itemId: "k",
			defVal: 100
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Fluid viscosity",
			shorttext: "\\mu",
			calculus: "cP",
			itemId: "mu",
			defVal: 1
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Fluid formation of volume factor",
			shorttext: "B",
			calculus: "rb/stb",
			itemId: "B",
			defVal: 1
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "External radius of well drainage area",
			shorttext: "r_e",
			calculus: "ft",
			itemId: "r_e",
			defVal: 100
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Well diameter",
			shorttext: "d_w",
			calculus: "in",
			itemId: "d_w",
			defVal: 9.4
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Skin-factor",
			shorttext: "S",
			nminValue: undefined,
			calculus: "\\blacktriangledown",
			itemId: "S",
			defVal: 0,
			llistener: {render: function(panel){
				Ext.getCmp('swindowV').alignTo(panel);
				panel.body.on('click', function() {
					Ext.getCmp('swindowV').alignTo(panel);
					if (Ext.getCmp('swindowV').isHidden() === false) {
						Ext.getCmp('swindowV').hide();
					}else{
						Ext.getCmp('swindowV').show();
						Ext.Function.defer(function() {Ext.EventManager.on(document, 'click', Ext.getCmp('swindowV').checkClick, Ext.getCmp('swindowV'));}, 20);
					}
				});
			}}
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "External (static) pressure",
			shorttext: "P_e",
			calculus: "psi",
			itemId: "P_e",
			defVal: 0
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Bottom hole flowing pressure",
			shorttext: "P_{wf}",
			calculus: "psi",
			itemId: "P_wf",
			defVal: 110
		},{
			xtype: 'formula',
			flex: 1, 
			width: "100%",
			itemId: "formula",
			formula: 'Q=\\dfrac{2 \\pi kh}{\\mu B \\left( \\ln\\left( \\dfrac{2r_e}{d_w}\\right) -0.5 + S \\right)} (P_i-P_{wf})',
			calcS: function(h, k, mu, B, re, dw, S, rws, ks, Pi, Pwf) {return (k/ks-1)*Math.log(2*rws/dw);},
			calc: function(h, k, mu, B, re, dw, S, rws, ks, Pi, Pwf) {return 2*3.1515926*k*h/(mu*B*(Math.log(2*re/dw)-0.5+S)) * (Pi-Pwf); }
			//formula: 'Q=\\dfrac{K_h h (P_i-P_{wf}) }{\\mu B \\left\\{ ln\\left[\\dfrac{a+\\sqrt{a^2-\\left(\\frac{L}{2}\\right)^2}}{\\frac{L}{2}}\\right]+\\dfrac{\\beta h}{L}ln\\dfrac{\\beta h}{(\\beta+1) r_c+1}+S \\right\\} }'
		},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 1000}]
    }],
    
    calcQ: function (button){
		var ihb = this.getComponent('itemholderbox');
		
		h = ihb.getComponent('h').getVal();
		k = ihb.getComponent('k').getVal();
		mu = ihb.getComponent('mu').getVal();
		B = ihb.getComponent('B').getVal();
		re = ihb.getComponent('r_e').getVal();
		dw = ihb.getComponent('d_w').getVal();
		S = ihb.getComponent('S').getVal();
		Pe = ihb.getComponent('P_e').getVal();
		Pwf = ihb.getComponent('P_wf').getVal();
		
		rws = Ext.getCmp('swindowV').getComponent('swindowbox').getComponent('textbox1').value;
		ks = Ext.getCmp('swindowV').getComponent('swindowbox').getComponent('textbox2').value;
		
		var main = Ext.ComponentQuery.query('appmain')[0];
		try{
			var imgS = main.getBackground();
		}catch (err) { 
			var imgS = ''; 
		}
		if (Pwf <= Pe){
			if (imgS.search('images/backgroundVP.png') == -1)
				main.setBackground('images/backgroundVP.png');
		}else{
			if (imgS.search('images/backgroundVM.png') == -1)
				main.setBackground('images/backgroundVM.png');
		}
		
		var answer = ihb.getComponent('formula').calc(h, k, mu, B, re, dw, S, 0, 0, Pe, Pwf);
		ihb.getComponent('formula').setAnswer(answer);
		
		if (rws != null && ks != null){
			answer = ihb.getComponent('formula').calcS(h, k, mu, B, re, dw, S, rws, ks, Pe, Pwf);
			ihb.getComponent('S').setVal(answer);
		}
	},
	
	calcS: function (){
		console.log('V');
		var ihb = this.getComponent('itemholderbox');
		
		h = ihb.getComponent('h').getVal();
		k = ihb.getComponent('k').getVal();
		mu = ihb.getComponent('mu').getVal();
		B = ihb.getComponent('B').getVal();
		re = ihb.getComponent('r_e').getVal();
		dw = ihb.getComponent('d_w').getVal();
		S = ihb.getComponent('S').getVal();
		Pe = ihb.getComponent('P_e').getVal();
		Pwf = ihb.getComponent('P_wf').getVal();
		
		rws = Ext.getCmp('swindowV').getComponent('swindowbox').getComponent('textbox1').value;
		ks = Ext.getCmp('swindowV').getComponent('swindowbox').getComponent('textbox2').value;
		
		var answer = ihb.getComponent('formula').calcS(h, k, mu, B, re, dw, S, rws, ks, Pe, Pwf);
		ihb.getComponent('S').setVal(answer);
	}
});
