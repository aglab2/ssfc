Ext.define('TestApp.view.main.Formula', {
	extend: 'Ext.Panel',
	xtype: 'formula',
	
	frame: false,
    border: false,
    bodyStyle: 'background:transparent',
    
    config: {   
         formula: '',
         calc: ''
    },
    
    listeners: {
		afterrender: function () {
			MathJax.Hub.Config({
				"HTML-CSS": {
					extensions: ["handle-floats.js"]
				}
			});
			MathJax.Hub.Queue(["Typeset",MathJax.Hub]);	
        } 
    },
    
    initComponent: function() {
		this.items = [{
        layout: {
		type:'hbox',
		align: 'stretch'
		},
		
		itemId: "formulaBox",
		frame: false,
		border: false,
		bodyStyle: 'background:transparent',
                        
        items: [{
			html: "<center>\\("+this.formula+"\\)</center>",
			id: 'formulaHTML',
			itemId: "formulahtml",
			align: 'stretch',
            xtype: 'panel',
			flex: 80,
            height: 100,
            frame: false,
			border: false,
			listeners : {
                'resize' : function(win,width,height,opt){
				   console.log(width);
				   console.log(height);
                   var fontsize = 0.002189781 * width + 0.001459854; //I know :)
                   console.log(fontsize);
                   console.log(fontsize+'em');
                   
                   this.body.setStyle('font-size', fontsize+'em');
                   this.setHeight(Math.round(95*width/361));
                 }
			},
			bodyStyle: {
				'background': '#ffffff', 
				'padding':'7px 0px 8px 0px',
				'border-radius':'5px 0px 0px 5px',
				'font-size': '0.7em'
			}
        },{xtype: 'panel', bodyStyle: 'background: transparent', flex: 3},{
            xtype: 'panel',
            html: '<center>=</center>',
			align: 'center',
			flex: 10,
            frame: false,
			border: false,
			bodyStyle: {
				'font-size': '3em',
				'background': '#ffffff',
				'padding':'30px 0px 7px 0px'
			}
        },{xtype: 'panel', bodyStyle: 'background: transparent', flex: 3},{
			html: "<center>"+""+"</center>",
            xtype: 'panel',
            itemId: 'answer',
			flex: 53,
            frame: false,
			border: false,
			bodyStyle: {
				'background': '#ffffff', 
				'padding':'30px 0px 8px 0px',
				'border-radius':'0px 5px 5px 0px'
			}
		}]
		}]	
		this.callParent();
	},
	
	setAnswer: function (answer){
		this.getComponent('formulaBox').getComponent('answer').update("<center>"+answer+"</center>");
	}
});
