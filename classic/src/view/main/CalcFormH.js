Ext.define('TestApp.view.main.ItemHolderH', {
	extend: 'Ext.form.Panel',
	xtype: 'itemholderH',
    position: 'fixed',

			frame: false,
                        border: false,
                        bodyStyle: 'background:transparent',

    requires: [
        'TestApp.view.main.Item'
    ],

    initComponent: function(){
		this.callParent();
		
		try{
			var main = Ext.ComponentQuery.query('appmain')[0];
			main.setBackground('images/backgroundHP.png');
		}catch(err) { };
	},

    items: [{
        layout: {type: 'vbox', align: 'stretch'},
		frame: false,
		border: false,
        bodyStyle: 'background:transparent',
        itemId: 'itemholderbox',

		defaults: {
			tlistener: {change: function(){
				var ih = this.ownerCt.ownerCt.ownerCt.ownerCt;
				ih.calcQ(this);
				this.focus(undefined, 20);
			}}
		},

        items: [{xtype: 'panel', bodyStyle: 'background: transparent', flex: 1000},{
			xtype: 'item',
			width: "100%",
			flex: 1,
			resizable: true,
			longtext: "Reservoir thickness",
			shorttext: "h",
			calculus: "ft",
			itemId: "h",
			defVal: 1
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Fluid viscosity",
			shorttext: "\\mu",
			calculus: "cP",
			itemId: "mu",
			defVal: 1
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Fluid formation of volume factor",
			shorttext: "B",
			calculus: "rb / stb",
			itemId: "B",
			defVal: 1
		},{
			
			xtype: 'item',
			width: "100%",
			flex: 1,
			resizable: true,
			longtext: "Horizontal length",
			shorttext: "L",
			calculus: "ft",
			itemId: "L",
			defVal: 1000
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "External radius of well drainage area",
			shorttext: "r_e",
			calculus: "ft",
			itemId: "r_e",
			defVal: 100
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Well diameter",
			shorttext: "d_w",
			calculus: "in",
			itemId: "d_w",
			defVal: 9.4
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Skin-factor",
			shorttext: "S",
			calculus: "\\blacktriangledown",
			itemId: "S",
			nminValue: undefined,
			defVal: 0,
			llistener: {render: function(panel){
				Ext.getCmp('swindowH').alignTo(panel);
				panel.body.on('click', function() {
					Ext.getCmp('swindowH').alignTo(panel);
					if (Ext.getCmp('swindowH').isHidden() === false) {
						Ext.getCmp('swindowH').hide();
					}else{
						Ext.getCmp('swindowH').show();
						Ext.Function.defer(function() {Ext.EventManager.on(document, 'click', Ext.getCmp('swindowH').checkClick, Ext.getCmp('swindowH'));}, 20);
					}
				});
			}}
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Horizontal Permeability",
			shorttext: "k_h",
			calculus: "mD",
			itemId: "k_h",
			defVal: 100
		},{
			xtype: 'item',
			width: "100%",
			flex: 1,
			resizable: true,
			longtext: "Vertical Permeability",
			shorttext: "k_v",
			calculus: "mD",
			itemId: "k_v",
			defVal: 10	
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "External (static) pressure",
			shorttext: "P_e",
			calculus: "psi",
			itemId: "P_e",
			defVal: 101
		},{
			xtype: 'item',
			flex: 1,
			width: "100%",
			longtext: "Bottom hole flowing pressure",
			shorttext: "P_{wf}",
			calculus: "psi",
			itemId: "P_wf",
			defVal: 1
		},{
			xtype: 'formula',
			flex: 1,
			width: "100%",
			itemId: "formula",
			formula: 'Q=\\dfrac{k_h h (P_i-P_{wf}) }{\\mu B \\left\\{ ln\\left[\\dfrac{a+\\sqrt{a^2-\\left(\\frac{L}{2}\\right)^2}}{\\frac{L}{2}}\\right]+\\dfrac{\\beta h}{L}ln\\dfrac{\\beta h}{(\\beta+1) r_e}+S \\right\\} }',
			
			calcS: function(h, L, kh, kv, mu, B, re, dw, S, Pe, Pwf, zw, Sv) {
				
				a = (L/2)*Math.sqrt(0.5 + Math.sqrt(0.25 + Math.pow(2 * re*h/L, 4) ));
				beta = Math.sqrt(kh/kv);
				
				S = Math.log(2*dw/L) + h/L*beta * Math.log(h*Math.sqrt(beta)/3.1415926/dw)/Math.sin(3.1415926*zw/h) + h/L*beta*Sv;
				return S;
			},
			calc: function(h, L, kh, kv, mu, B, re, dw, S, Pe, Pwf) 
				{ 
					a = (L/2)*Math.sqrt(0.5 + Math.sqrt(0.25 + Math.pow(2 * re*h/L, 4) ));
					beta = Math.sqrt(kh/kv);
			Q = kh*h*(Pe-Pwf)/(mu*B*(Math.log(a + Math.sqrt(a*a - L*L/4)/(L/2) ) + (beta*h/L)*Math.log(beta * h / (beta + 1)*re) + S ));
 return Q; } 
			//formula: 'Q=\\dfrac{K_h h (P_i-P_{wf}) }{\\mu B \\left\\{ ln\\left[\\dfrac{a+\\sqrt{a^2-\\left(\\frac{L}{2}\\right)^2}}{\\frac{L}{2}}\\right]+\\dfrac{\\beta h}{L}ln\\dfrac{\\beta h}{(\\beta+1) r_c+1}+S \\right\\} }'
		},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 1000}]
    }],


    calcS: function (){
		console.log('H');
		var ihb = this.getComponent('itemholderbox');

		h = ihb.getComponent('h').getVal();
		mu = ihb.getComponent('mu').getVal();
		kh = ihb.getComponent('k_h').getVal();
		kv = ihb.getComponent('k_v').getVal();
		L = ihb.getComponent('L').getVal();
		B = ihb.getComponent('B').getVal();
		re = ihb.getComponent('r_e').getVal();
		dw = ihb.getComponent('d_w').getVal();
		S = ihb.getComponent('S').getVal();
		Pe = ihb.getComponent('P_e').getVal();
		Pwf = ihb.getComponent('P_wf').getVal();
		
		zw = Ext.getCmp('swindowH').getComponent('swindowbox').getComponent('textbox1').value;
		Sv = Ext.getCmp('swindowH').getComponent('swindowbox').getComponent('textbox2').value;
		
		var answer = ihb.getComponent('formula').calcS(h, L, kh, kv, mu, B, re, dw, S, Pe, Pwf, zw, Sv);
		ihb.getComponent('S').setVal(answer);
	},

    calcQ: function (){
		var ihb = this.getComponent('itemholderbox');

		h = ihb.getComponent('h').getVal();
		mu = ihb.getComponent('mu').getVal();
		kh = ihb.getComponent('k_h').getVal();
		kv = ihb.getComponent('k_v').getVal();
		L = ihb.getComponent('L').getVal();
		B = ihb.getComponent('B').getVal();
		re = ihb.getComponent('r_e').getVal();
		dw = ihb.getComponent('d_w').getVal();
		S = ihb.getComponent('S').getVal();
		Pe = ihb.getComponent('P_e').getVal();
		Pwf = ihb.getComponent('P_wf').getVal();
		
		zw = Ext.getCmp('swindowH').getComponent('swindowbox').getComponent('textbox1').value;
		Sv = Ext.getCmp('swindowH').getComponent('swindowbox').getComponent('textbox2').value;
		
		var main = Ext.ComponentQuery.query('appmain')[0];
		var imgS = main.getBackground();
		if (Pwf <= Pe){
			if (imgS.search('images/backgroundHP.png') == -1)
				main.setBackground('images/backgroundHP.png');
		}else{
			if (imgS.search('images/backgroundHM.png') == -1)
				main.setBackground('images/backgroundHM.png');
		}
		
		var answer = ihb.getComponent('formula').calc(h, L, kh, kv, mu, B, re, dw, S, Pe, Pwf);
		ihb.getComponent('formula').setAnswer(answer);
		
		console.log(zw, Sv);
		if (zw != null && Sv != null){
			var answer = ihb.getComponent('formula').calcS(h, L, kh, kv, mu, B, re, dw, S, Pe, Pwf, zw, Sv);
			ihb.getComponent('S').setVal(answer);
		}
	}
});
