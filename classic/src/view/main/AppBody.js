Ext.define('TestApp.view.main.AppBody', {
	extend: 'Ext.Panel',
	xtype: 'appbody',
	require: [
	'TestApp.view.main.SwitchEntry'],
	layout: 'hbox',
	
	frame: false,
    border: false,
    bodyStyle: 'background: transparent;',

	items: [
		{
			xtype: 'itemholderV',
			flex: 20,
			frame: false,
       		border: false,
       		id: 'itemholder',
        	bodyStyle: 'background:transparent'
		},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 6},{
			xtype: 'switcherentry',
			id: 'buttons',
			flex: 3
		},{
		xtype:'componentlabel', flex: 4},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 10}		
	]
});
