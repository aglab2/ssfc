Ext.define('TestApp.view.main.SWindow', {
	extend: 'Ext.window.Window',
	xtype: 'swindow',
	closable: false,
	resizable: false,
    draggable: false,
    config: {   
		label1: '',
        label2: '',
        calculus1: '',
        calculus2: '',
        listener1: [],
        listener2: []
    },
    
    initComponent: function(){
		this.items = [{
			itemId: "swindowbox",
			xtype: 'form',
			bodyStyle: 'margin: 5px;',
			items: [{
				itemId: "textbox1",
				xtype: 'numberfield',
				hideTrigger: true,
				keyNavEnabled: false,
				mouseWheelEnabled: false,
				minValue: 0,
				fieldLabel: this.label1,
				name: this.label1,
				listeners: this.listener1
			},{
				itemId: "textbox2",
				xtype: 'numberfield',
				hideTrigger: true,
				keyNavEnabled: false,
				mouseWheelEnabled: false,
				minValue: 0,
				fieldLabel: this.label2,
				name: this.label2,
				listeners: this.listener2
			}]
		}];
		
		this.callParent();
	},
	
    renderTo: Ext.getBody(),

	checkClick : function(event) {
		if (!event.within(this.getEl())) {
			this.hided();
		}
	},
	
	hided : function() {
		console.log('hiding');
		Ext.EventManager.un(document, 'click', this.checkClick, this);
		this.hide();
	}
});
