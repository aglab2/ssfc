Ext.define('TestApp.view.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'appmain',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'TestApp.view.main.MainController',
        'TestApp.view.main.MainModel',
        'TestApp.view.main.List',
		'TestApp.view.main.AppBody',
		'TestApp.view.main.Formula',
		'TestApp.view.main.Switcher'
    ],
    controller: 'main',
    viewModel: 'main',
    
    initComponent: function(){
		Ext.getDoc().on('keydown', function (e, t) {
			if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {
				e.stopEvent();
			}
		});

		Ext.get('loading-parent').setVisible(false);
		Ext.get('loading-parent').hide();
		Ext.get('loading-mask').setVisible(false);
		Ext.get('loading-mask').hide();
		
		var swindowV = Ext.create('TestApp.view.main.SWindow',{
			id: 'swindowV', 
			label1: 'Damage zone radius',
			label2: 'Damage zone permeability',
			listener1: {
				change: function(){
					var ih = Ext.getCmp('itemholder');
					ih.calcS();
					this.focus(undefined, 20);
				}
			},
			listener2: {
				change: function(){
					var ih = Ext.getCmp('itemholder');
					ih.calcS();
					this.focus(undefined, 20);
				}
			}
		}).show().hide(); //I know :)
		var swindowH = Ext.create('TestApp.view.main.SWindow',{
			id: 'swindowH',
			label1: 'Distance from bottom formation to horizontal well',
			label2: 'Vertical Skin-factor',
			listener1: {
				change: function(){
					var ih = Ext.getCmp('itemholder');
					ih.calcS();
					this.focus(undefined, 20);
				}
			},
			listener2: {
				change: function(){
					var ih = Ext.getCmp('itemholder');
					ih.calcS();
					this.focus(undefined, 20);
				}
			}
		}).show().hide(); //I know :)
		
		this.callParent();
		//Ext.getCmp('itemholder').calcQ(thos);
	},
	
	overflowY: 'scroll',
    
    items: [{
		overflowX: 'scroll',
		xtype: "container",
		enable: false,
		height: "100%",
		minHeight: 1300,
        minWidth: 1100,
        itemId: "mainbox",
        focusFrame: false,
		
        html:'<img src="images/backgroundVM.png" style="width: 100%" />',
        
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
		items: [{xtype:'panel', flex: 1.5, width: "100%", 
			'bodyStyle':'background: transparent'},
		{
            xtype: 'appbody',
			padding: '0 0 0 100em',
            flex: 8,
            width: "100%",
			id: "appbody"
		},{xtype:'panel', flex: 0, width: "100%", 'bodyStyle':'background: transparent'}]
    }],
    
    setBackground: function(bg_name){
		this.getComponent('mainbox').update('<img src="'+bg_name+'" style="width: 100%" />', false);
	},
	getBackground: function(bg_name){
		return this.getComponent('mainbox').getEl().dom.innerHTML;
	}
});
