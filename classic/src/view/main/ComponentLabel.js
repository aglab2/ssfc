Ext.define('TestApp.view.main.ComponentLabel', {
	extend: 'Ext.Panel',
	xtype: 'componentlabel',
	height: '40px',
	visible:false,
	require: ['TestApp.view.main.Panel'],
    bodyStyle: 'background: transparent; border-radius: 100px 100px 100px 100px',
	layout:{type:'hbox', align: 'stretch'},
	items: [{cls: 'complabel',xtype: 'panel',bodyStyle: 'background: green', width: '40px'},
		{xtype: 'panel', flex: 6 }]
});
	
