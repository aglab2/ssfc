Ext.define('TestApp.view.main.Switcher', {
	extend: 'Ext.form.Panel',
	xtype: 'switcher',		    
    config: {
		 sname: '',
         choice1: '',
         choice2: '',
         listener1: [],
         listener2: []
    },
    
	frame: false,
    border: false,
    bodyStyle: 'background:transparent',
    
    initComponent: function() {
		this.items = [{
			xtype: 'fieldcontainer',
			itemId: 'switcherbox',
			layout: {
				type: 'vbox',
				align: 'center'
			},
		
			width: "100%",
			frame: false,
			border: false,
			bodyStyle: 'background:transparent',

			items: [{
					html: "<center><font color=\"white\">"+this.sname+"</font></center>",
					xtype: 'label',
					flex: 1,
					bodyStyle: {
						'background': 'transparent'
					}
				},{
					text: this.choice1,
					xtype: 'button',
					itemId: 'button1',
					flex: 1,
					width: "100%",
					bodyStyle: 'border: 0 0 0 0',
					toggleGroup: this.sname,
					pressed: true,
					allowDepress: false,
					listeners: this.listener1
				},{
					text: this.choice2,
					xtype: 'button',
					itemId: 'button2',
					flex: 1,
					width: "100%",
					bodyStyle: 'border: none;',
					toggleGroup: this.sname,
					allowDepress: false,
					listeners: this.listener2
				}]
		}],		
		this.callParent();
	},
	
	getVal: function (){
		return this.getComponent('switcherbox').getComponent('button1').pressed;
	}
});
