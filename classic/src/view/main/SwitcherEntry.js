Ext.define('TestApp.view.main.SwitchEntry', {
	extend: 'Ext.Panel',
	xtype: 'switcherentry',
	require: ['TestApp.view.main.SwitchButtonSegment'],
	
	frame: false,
    border: false,
    bodyStyle: 'background: transparent;',

	items: [
		{
			xtype: 'switcher',
            choice1: 'Vertical',
            choice2: 'Horizontal',
            sname: 'Well Type',
			itemId: 'switcher1',
			flex: 3,
			listener1: {
				toggle: function(b, pd) {
					if (!pd) return;
					Ext.getBody().mask('Loading...', 'x-mask-loading', false);
					
					Ext.Function.defer(function() {
						var appbody = Ext.ComponentQuery.query('appbody')[0];
						appbody.remove(0);
						appbody.insert(0,
						{
							xtype: 'itemholderV',
							flex: 20,
							frame: false,
							border: false,
							id: "itemholder",
							bodyStyle: 'background:transparent'
						});
						if (!oil){
							vertical = Ext.getCmp('buttons').getComponent('switcher1').getVal();
						var holder = Ext.getCmp('itemholder');

						holder.getComponent('itemholderbox').getComponent('h').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('h').getVal();
						holder.getComponent('itemholderbox').getComponent('h').setVal(a/3.281);

						holder.getComponent('itemholderbox').getComponent('B').set('calculus', 'rm^3/stm');

						if (!vertical) {
						holder.getComponent('itemholderbox').getComponent('L').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('L').getVal();
						holder.getComponent('itemholderbox').getComponent('L').setVal(a/3.281);}

						holder.getComponent('itemholderbox').getComponent('r_e').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('r_e').getVal();
						holder.getComponent('itemholderbox').getComponent('r_e').setVal(a/3.281);

						holder.getComponent('itemholderbox').getComponent('d_w').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('d_w').getVal();
						holder.getComponent('itemholderbox').getComponent('d_w').setVal(a/39.37);
	
						holder.getComponent('itemholderbox').getComponent('P_e').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_e').getVal();
						holder.getComponent('itemholderbox').getComponent('P_e').setVal(a/0.145);

						holder.getComponent('itemholderbox').getComponent('P_wf').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_wf').getVal();
						holder.getComponent('itemholderbox').getComponent('P_wf').setVal(a/0.145);
			
						}
						Ext.getBody().unmask(false);
					}, 10);
				}
			},
			listener2: {
				toggle: function(b, pd) {
					if (!pd) return;
					Ext.getBody().mask('Loading...', 'x-mask-loading', false);
					
					Ext.Function.defer(function() {
						var appbody = Ext.ComponentQuery.query('appbody')[0];
						appbody.remove(0);
						appbody.insert(0,
						{
							xtype: 'itemholderH',
							flex: 20,
							frame: false,
							border: false,
							id: "itemholder",
							bodyStyle: 'background:transparent'
						});
						oil = Ext.getCmp('buttons').getComponent('switcher2').getVal();
						if (!oil){
							vertical = Ext.getCmp('buttons').getComponent('switcher1').getVal();
						var holder = Ext.getCmp('itemholder');

						holder.getComponent('itemholderbox').getComponent('h').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('h').getVal();
						holder.getComponent('itemholderbox').getComponent('h').setVal(a);

						holder.getComponent('itemholderbox').getComponent('B').set('calculus', 'rm^3/stm');

						if (!vertical) {
						holder.getComponent('itemholderbox').getComponent('L').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('L').getVal();
						holder.getComponent('itemholderbox').getComponent('L').setVal(a);}

						holder.getComponent('itemholderbox').getComponent('r_e').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('r_e').getVal();
						holder.getComponent('itemholderbox').getComponent('r_e').setVal(a);

						holder.getComponent('itemholderbox').getComponent('d_w').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('d_w').getVal();
						holder.getComponent('itemholderbox').getComponent('d_w').setVal(a);
	
						holder.getComponent('itemholderbox').getComponent('P_e').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_e').getVal();
						holder.getComponent('itemholderbox').getComponent('P_e').setVal(a);

						holder.getComponent('itemholderbox').getComponent('P_wf').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_wf').getVal();
						holder.getComponent('itemholderbox').getComponent('P_wf').setVal(a);
			
						}
						Ext.getBody().unmask(false);
					}, 10);
				}
			}
		},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 2},{
			xtype: 'switcher',
            choice1: 'Oil',
            choice2: 'Metric',
            sname: 'Units',
			itemId: 'switcher2',
			flex: 3,
			listener1: {
				toggle: function(b, pd) {
					if (!pd) return;
					Ext.getBody().mask('Loading...', 'x-mask-loading', false);
					
					Ext.Function.defer(function() {
						var appbody = Ext.ComponentQuery.query('appbody')[0];
						vertical = Ext.getCmp('buttons').getComponent('switcher1').getVal();
						var holder = Ext.getCmp('itemholder');

						holder.getComponent('itemholderbox').getComponent('h').set('calculus', 'ft');
						a = holder.getComponent('itemholderbox').getComponent('h').getVal();
						holder.getComponent('itemholderbox').getComponent('h').setVal(a/3.281);

						holder.getComponent('itemholderbox').getComponent('B').set('calculus', 'rb/stb');

						if (!vertical) {
						holder.getComponent('itemholderbox').getComponent('L').set('calculus', 'ft');
						a = holder.getComponent('itemholderbox').getComponent('L').getVal();
						holder.getComponent('itemholderbox').getComponent('L').setVal(a/3.281);}

						holder.getComponent('itemholderbox').getComponent('r_e').set('calculus', 'ft');
						a = holder.getComponent('itemholderbox').getComponent('r_e').getVal();
						holder.getComponent('itemholderbox').getComponent('r_e').setVal(a/3.281);

						holder.getComponent('itemholderbox').getComponent('d_w').set('calculus', 'in');
						a = holder.getComponent('itemholderbox').getComponent('d_w').getVal();
						holder.getComponent('itemholderbox').getComponent('d_w').setVal(a/39.37);
	
						holder.getComponent('itemholderbox').getComponent('P_e').set('calculus', 'psi');
						a = holder.getComponent('itemholderbox').getComponent('P_e').getVal();
						holder.getComponent('itemholderbox').getComponent('P_e').setVal(a/0.145);

						holder.getComponent('itemholderbox').getComponent('P_wf').set('calculus', 'psi');
						a = holder.getComponent('itemholderbox').getComponent('P_wf').getVal();
						holder.getComponent('itemholderbox').getComponent('P_wf').setVal(a/0.145);

						Ext.getBody().unmask(false);
					}, 10);
				}
			},
			listener2: {
				toggle: function(b, pd) {
					if (!pd) return;
					Ext.getBody().mask('Loading...', 'x-mask-loading', false);
					
					Ext.Function.defer(function() {
						vertical = Ext.getCmp('buttons').getComponent('switcher1').getVal();
						var holder = Ext.getCmp('itemholder');

						holder.getComponent('itemholderbox').getComponent('h').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('h').getVal();
						holder.getComponent('itemholderbox').getComponent('h').setVal(a*3.281);

						holder.getComponent('itemholderbox').getComponent('B').set('calculus', 'rm^3/stm');

						if (!vertical) {
						holder.getComponent('itemholderbox').getComponent('L').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('L').getVal();
						holder.getComponent('itemholderbox').getComponent('L').setVal(a*3.281);}

						holder.getComponent('itemholderbox').getComponent('r_e').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('r_e').getVal();
						holder.getComponent('itemholderbox').getComponent('r_e').setVal(a*3.281);

						holder.getComponent('itemholderbox').getComponent('d_w').set('calculus', 'm');
						a = holder.getComponent('itemholderbox').getComponent('d_w').getVal();
						holder.getComponent('itemholderbox').getComponent('d_w').setVal(a*39.37);
	
						holder.getComponent('itemholderbox').getComponent('P_e').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_e').getVal();
						holder.getComponent('itemholderbox').getComponent('P_e').setVal(a*0.145);

						holder.getComponent('itemholderbox').getComponent('P_wf').set('calculus', 'kPa');
						a = holder.getComponent('itemholderbox').getComponent('P_wf').getVal();
					holder.getComponent('itemholderbox').getComponent('P_wf').setVal(a*0.145);			
						Ext.getBody().unmask(false);
					}, 10);
				}
			}
		},{xtype: 'panel', bodyStyle: 'background: transparent', flex: 1},{
			xtype: 'switcher',
            choice1: 'SS',
            choice2: 'PSS',
            listener1: {
				toggle: function (b, pd) {	
					if(!Ext.getCmp('buttons').getComponent('switcher1').getVal()) return;
					
					var formula = Ext.getCmp('itemholder').getComponent('itemholderbox').getComponent('formula');
					formula.formula = 'Q=\\dfrac{2 \\pi kh}{\\mu B \\left( \\ln\\left( \\dfrac{2r_e}{d_w}\\right) -0.75 + S \\right)} (P_i-P_{wf})';
					formula.calc = function(h, k, mu, B, re, dw, S, rws, ks, Pi, Pwf) {return 2*3.1515926*k*h/(mu*B*(Math.log(2*re/dw)-0.75+S)) * (Pi-Pwf); }
					formula.getComponent('formulaBox').getComponent('formulahtml').update("<center>\\("+formula.formula+"\\)</center>");
					MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
					Ext.getCmp('itemholder').calcQ();
				}
			},
			listener2: {
				toggle: function (b, pd) {	
					if(!Ext.getCmp('buttons').getComponent('switcher1').getVal()) return;
					
					var formula = Ext.getCmp('itemholder').getComponent('itemholderbox').getComponent('formula');
					formula.formula = 'Q=\\dfrac{2 \\pi kh}{\\mu B \\left( \\ln\\left( \\dfrac{2r_e}{d_w}\\right) -0.5 + S\\right) } (P_i-P_{wf})';
					formula.calc = function(h, k, mu, B, re, dw, S, rws, ks, Pi, Pwf) {return 2*3.1515926*k*h/(mu*B*(Math.log(2*re/dw)-0.5+S)) * (Pi-Pwf); }
					formula.getComponent('formulaBox').getComponent('formulahtml').update("<center>\\("+formula.formula+"\\)</center>");
					MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
					Ext.getCmp('itemholder').calcQ();
				}
			},
            sname: 'Drainage <br> conditions',
			flex: 3
		},
	]
});

